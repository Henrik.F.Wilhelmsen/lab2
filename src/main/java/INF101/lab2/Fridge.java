package INF101.lab2;

import java.util.*;

public class Fridge implements IFridge{

    List<FridgeItem> fridgeStorage;
    int capacity;

    public Fridge(){
        fridgeStorage = new ArrayList<>();
        capacity = 20;
    }

    @Override
    public int nItemsInFridge() {
        return fridgeStorage.size();
    }

    @Override
    public int totalSize() {
        return capacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (fridgeStorage.size() < this.capacity) {
            fridgeStorage.add(item);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeStorage.contains(item)){
            fridgeStorage.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridgeStorage.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired = new ArrayList<>();

        for (FridgeItem i : fridgeStorage) {
            if (i.hasExpired()) {
                expired.add(i);
            }
        }

        for (FridgeItem e : expired) {
            fridgeStorage.remove(e);
        }
        return expired;
    }
}
